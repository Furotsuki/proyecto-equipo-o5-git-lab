/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Consultorio.Implement;

import Consultorio.Dao.UsuarioDao;
import Consultorio.Model.Usuario;
import Consultorio.Services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
@Service
/**
 *
 * @author Furotsuki
 */
public class UsuarioImplements implements UsuarioService{

    @Autowired
    private UsuarioDao usuarioDao;
    //AGREGAR - ACTUALIZAR
    @Override
    @Transactional(readOnly=false)
    public Usuario save(Usuario usuario)
    {
    return usuarioDao.save(usuario);
    }
    //BORRAR
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id)
    {
    usuarioDao.deleteById(id);
    }
    //BUSCAR POR ID
    @Override
    @Transactional(readOnly=true)
    public Usuario findById(Integer id)
    {
    return usuarioDao.findById(id).orElse(null);
    }
    //BUSCAR TODOS
    @Override
    @Transactional(readOnly=true)
    public List<Usuario> findAll()
    {
    return (List<Usuario>) usuarioDao.findAll();

    }
}
