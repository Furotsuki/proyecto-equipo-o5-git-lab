/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Consultorio.Controllers;


import java.util.List;
import Consultorio.Model.Usuario;
import Consultorio.Services.UsuarioService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 *
 * @author Furotsuki
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/usuario")

public class UsuarioController {
    @Autowired
    private UsuarioService usuarioservice;

//Agregar

@PostMapping(value="/")
public ResponseEntity<Usuario> agregar(@RequestBody Usuario usuario){
Usuario obj = usuarioservice.save(usuario);
return new ResponseEntity<>(obj, HttpStatus.OK);
}

//Editar
@PutMapping(value="/")
public ResponseEntity<Usuario> editar(@RequestBody Usuario usuario){
Usuario obj = usuarioservice.findById(usuario.getId());
if(obj!=null)
{
 obj.setCedula(usuario.getCedula());
 obj.setUsuario(usuario.getUsuario());
 obj.setNombre(usuario.getNombre());
 obj.setFecha_nacimiento(usuario.getFecha_nacimiento());
 obj.setCorreo(usuario.getCorreo());
 obj.setClave(usuario.getClave());
 obj.setActivo(usuario.getActivo());
usuarioservice.save(obj);
}
else
 return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
return new ResponseEntity<>(obj, HttpStatus.OK);
}


//Borrar
@DeleteMapping(value="/{id}")
public ResponseEntity<Usuario> eliminar(@PathVariable Integer id){
Usuario obj = usuarioservice.findById(id);
if(obj!=null)
 usuarioservice.delete(id);
else
 return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
return new ResponseEntity<>(obj, HttpStatus.OK);
}

//Buscar por Id
@GetMapping("/list/{id}")
public Usuario consultaPorId(@PathVariable Integer id){
return usuarioservice.findById(id);
}

//Buscar Todo
@GetMapping("list/")
public List<Usuario> consultarTodo(){
return usuarioservice.findAll();
}

}
