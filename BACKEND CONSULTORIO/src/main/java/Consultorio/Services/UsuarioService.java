/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Consultorio.Services;

import Consultorio.Model.Usuario;
import java.util.List;
/**
 *
 * @author Furotsuki
 */
public interface UsuarioService {
    //AGREGAR O INSERTAR
    public Usuario save(Usuario Usuario );
    //BORRAR
    public void delete(Integer id);
    //BUSCAR POR ID
    public Usuario findById(Integer id);
    //BUSCAR TODOS
    public List<Usuario> findAll();
}
