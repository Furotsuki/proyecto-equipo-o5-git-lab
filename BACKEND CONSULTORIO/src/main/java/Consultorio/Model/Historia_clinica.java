/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Consultorio.Model;

/**
 *
 * @author Furotsuki
 */
public class Historia_clinica {
    private int id;
    private int id_cita;
    private String descripsion;
    private String medicamentos_suministrados;

    public Historia_clinica() {
    }

    public Historia_clinica(int id, int id_cita, String descripsion, String medicamentos_suministrados) {
        this.id = id;
        this.id_cita = id_cita;
        this.descripsion = descripsion;
        this.medicamentos_suministrados = medicamentos_suministrados;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_cita() {
        return id_cita;
    }

    public void setId_cita(int id_cita) {
        this.id_cita = id_cita;
    }

    public String getDescripsion() {
        return descripsion;
    }

    public void setDescripsion(String descripsion) {
        this.descripsion = descripsion;
    }

    public String getMedicamentos_suministrados() {
        return medicamentos_suministrados;
    }

    public void setMedicamentos_suministrados(String medicamentos_suministrados) {
        this.medicamentos_suministrados = medicamentos_suministrados;
    }



}
