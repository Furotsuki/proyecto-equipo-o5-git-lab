/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Consultorio.Model;

import java.sql.Time;
import java.util.Date;

/**
 *
 * @author Furotsuki
 */
public class Citas {
    private int id;
    private int id_paciente;
    private int id_usuario;
    private int id_medico;
    private int id_historia;
    private Date fecha_cita;
    private Time hora_cita;
    private boolean cancelado;

    public Citas() {
    }

    public Citas(int id, int id_paciente, int id_usuario, int id_medico, int id_historia, Date fecha_cita, Time hora_cita, boolean cancelado) {
        this.id = id;
        this.id_paciente = id_paciente;
        this.id_usuario = id_usuario;
        this.id_medico = id_medico;
        this.id_historia = id_historia;
        this.fecha_cita = fecha_cita;
        this.hora_cita = hora_cita;
        this.cancelado = cancelado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_paciente() {
        return id_paciente;
    }

    public void setId_paciente(int id_paciente) {
        this.id_paciente = id_paciente;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_medico() {
        return id_medico;
    }

    public void setId_medico(int id_medico) {
        this.id_medico = id_medico;
    }

    public int getId_historia() {
        return id_historia;
    }

    public void setId_historia(int id_historia) {
        this.id_historia = id_historia;
    }

    public Date getFecha_cita() {
        return fecha_cita;
    }

    public void setFecha_cita(Date fecha_cita) {
        this.fecha_cita = fecha_cita;
    }

    public Time getHora_cita() {
        return hora_cita;
    }

    public void setHora_cita(Time hora_cita) {
        this.hora_cita = hora_cita;
    }

    public boolean isCancelado() {
        return cancelado;
    }

    public void setCancelado(boolean cancelado) {
        this.cancelado = cancelado;
    }




}
