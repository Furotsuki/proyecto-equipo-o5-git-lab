/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Consultorio.Model;

import java.sql.Time;

/**
 *
 * @author Furotsuki
 */
public class Especialistas {
    private int id_doctor;
    private String nombre;
    private String especialidad;
    private long telefono;
    private Time horarios;
    private boolean activo;

    public Especialistas() {
    }

    public Especialistas(int id_doctor, String nombre, String especialidad, long telefono, Time horarios, boolean activo) {
        this.id_doctor = id_doctor;
        this.nombre = nombre;
        this.especialidad = especialidad;
        this.telefono = telefono;
        this.horarios = horarios;
        this.activo = activo;
    }

    public int getId_doctor() {
        return id_doctor;
    }

    public void setId_doctor(int id_doctor) {
        this.id_doctor = id_doctor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public long getTelefono() {
        return telefono;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }

    public Time getHorarios() {
        return horarios;
    }

    public void setHorarios(Time horarios) {
        this.horarios = horarios;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }



}
