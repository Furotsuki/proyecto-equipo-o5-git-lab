/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Consultorio.Dao;

import Consultorio.Model.Usuario;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Furotsuki
 */
public interface UsuarioDao extends CrudRepository<Usuario,Integer> {
    
}
